package com.imaknow.calculatebmi.ui.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.CalculateBmiApplication;
import com.imaknow.calculatebmi.misc.MyAsyncTask;
import com.imaknow.calculatebmi.misc.OnDoInBackground;
import com.imaknow.calculatebmi.misc.OnDoInPostExecute;
import com.imaknow.calculatebmi.presenter.SelectUnitsPresenter;
import com.imaknow.calculatebmi.ui.pager.PagerFragment;
import com.imaknow.calculatebmi.ui.pager.PagerWidget;
import com.imaknow.calculatebmi.ui.pager.UnitFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Main fragment.
 */
public class SelectUnitsFragment extends Fragment {
    private SelectUnitsPresenter mSelectUnitsPresenter;
    private View mRootView;
    private List<PagerFragment> mUnitFragmentList;
    private PagerWidget mUnitPager;
    private boolean mIsCurrentUnitKgAndCm;
    private boolean mOpenedFirstTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mSelectUnitsPresenter = new SelectUnitsPresenter(this);
        generateUnitContent();

        // Store this at the very beginning because later will be updated.
        mOpenedFirstTime = mSelectUnitsPresenter.isOpenedFirstTime();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_select_unit, container, false);

        initUnitPager();

        return mRootView;
    }

    private void initUnitPager() {
        mUnitPager = (PagerWidget) mRootView.findViewById(R.id.unit_pager_widget);
        mUnitPager.setList(getActivity().getSupportFragmentManager(), mUnitFragmentList);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateDataInPager();
    }

    private void generateUnitContent() {
        UnitFragment kgCmFragment = new UnitFragment();
        Bundle args = new Bundle();
        args.putString(CalculateBmiApplication.BUNDLE_UNIT_NAME, getString(R.string.kg_cm));
        kgCmFragment.setArguments(args);

        UnitFragment poundInchFragment = new UnitFragment();
        Bundle args2 = new Bundle();
        args2.putString(CalculateBmiApplication.BUNDLE_UNIT_NAME, getString(R.string.pound_inch));
        poundInchFragment.setArguments(args2);

        mUnitFragmentList = new ArrayList<>();
        mUnitFragmentList.add(kgCmFragment);
        mUnitFragmentList.add(poundInchFragment);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_ok, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ok:
                String unitSelected = mUnitFragmentList.get(mUnitPager.getCurrentItemPosition()).getCurrentValue();
                mSelectUnitsPresenter.saveUnitByValue(unitSelected);
                if (mOpenedFirstTime) {
                    mSelectUnitsPresenter.openMainFragment(getActivity(), true);
                } else {
                    getActivity().onBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isOpenedFirstTime() {
        return mOpenedFirstTime;
    }

    /**
     * Obtains the saved value and puts it in the pager.
     */
    private void populateDataInPager() {
        new MyAsyncTask(new OnDoInBackground() {
            @Override
            public void doInBackground() {
                mIsCurrentUnitKgAndCm = mSelectUnitsPresenter.isCurrentUnitKgAndCm();
            }
        }, new OnDoInPostExecute() {
            @Override
            public void doInPostExecute() {
                if (mIsCurrentUnitKgAndCm) {
                    mUnitPager.setPage(0);
                } else {
                    mUnitPager.setPage(1);
                }
            }
        }).execute();
    }
}
