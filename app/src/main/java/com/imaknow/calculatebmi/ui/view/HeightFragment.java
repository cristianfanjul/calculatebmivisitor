package com.imaknow.calculatebmi.ui.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.presenter.HeightPresenter;
import com.imaknow.calculatebmi.ui.custom.VerticalSeekBar;

/**
 * Height selection fragment.
 */
public class HeightFragment extends Fragment {
    private View mRootView;
    private HeightPresenter mHeightPresenter;
    private TextView mTxtVwHeight;
    private ImageView mImgVwBody;
    private VerticalSeekBar mSeekBarHeight;
    private RelativeLayout.LayoutParams mOrigParams;
    private int mOrigHeight;
    private float mImageIncreaseFactor;
    private int mKeyDownY = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mHeightPresenter = new HeightPresenter(this);
        mImageIncreaseFactor = getResources().getInteger(R.integer.increase_height_body_factor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_height, container, false);
        initViews();
        return mRootView;
    }

    private void initViews() {
        mTxtVwHeight = (TextView) mRootView.findViewById(R.id.txt_vw_height);
        mTxtVwHeight.setTypeface(mHeightPresenter.getTypeface(getActivity()));
        mImgVwBody = (ImageView) mRootView.findViewById(R.id.img_vw_body);
        mSeekBarHeight = (VerticalSeekBar) mRootView.findViewById(R.id.seek_bar_height);
        attachGenderImage();
        mOrigParams = (RelativeLayout.LayoutParams) mImgVwBody.getLayoutParams();
        mOrigHeight = mOrigParams.height;
        int savedProgress = mHeightPresenter.getProgressFromHeight();
        mSeekBarHeight.setProgress(savedProgress);
        updateHeightText(savedProgress);
        updateImageHeight(savedProgress);
        mSeekBarHeight.setOnSeekBarChangeListener(getSeekBarChangeListener());

        mImgVwBody.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mKeyDownY = (int) event.getY();
                        Log.d("Event", "Event DOWN Y" + mKeyDownY);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float y = (int) event.getY();
                        int currentProgress = mSeekBarHeight.getProgress();
                        Log.d("Event", "Event MOVE Y" + y);
                        if (Math.abs(y) % 2 == 0) {
                            if (y > mKeyDownY) {
                                mSeekBarHeight.setProgress(currentProgress - 1);
                            } else if (y < mKeyDownY) {
                                mSeekBarHeight.setProgress(currentProgress + 1);
                            }
                            mSeekBarHeight.updateThumb();
                        }
                }
                return false;
            }
        });
    }

    @NonNull
    private SeekBar.OnSeekBarChangeListener getSeekBarChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("Event", "Progress changed to: " + progress);
                updateImageHeight(progress);
                updateHeightText(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
    }

    private void attachGenderImage() {
        if (mHeightPresenter.isMale()) {
            mImgVwBody.setImageDrawable(getResources().getDrawable(R.drawable.male_body));
            mTxtVwHeight.setBackgroundResource(R.drawable.height_text_shape_male);
            mSeekBarHeight.setThumb(getResources().getDrawable(R.drawable.male_scrubber_control_selector_holo_light));
            mSeekBarHeight.setProgressDrawable(getResources().getDrawable(R.drawable.male_scrubber_progress_horizontal_holo_light));
        } else {
            mImgVwBody.setImageDrawable(getResources().getDrawable(R.drawable.female_body));
            mTxtVwHeight.setBackgroundResource(R.drawable.height_text_shape_female);
            mSeekBarHeight.setThumb(getResources().getDrawable(R.drawable.female_scrubber_control_selector_holo_light));
            mSeekBarHeight.setProgressDrawable(getResources().getDrawable(R.drawable.female_scrubber_progress_horizontal_holo_light));
        }
    }

    private void updateImageHeight(int progress) {
        mOrigParams.height = mOrigHeight + (int) (progress * (mImageIncreaseFactor / 10));
        mImgVwBody.setLayoutParams(mOrigParams);
        mHeightPresenter.saveHeight(mHeightPresenter.getCalculatedHeight(progress));
    }

    private void updateHeightText(int progress) {
        mTxtVwHeight.setText(mHeightPresenter.getCalculatedHeight(progress)
                + " " + mHeightPresenter.getUnitType());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_back_next, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                mHeightPresenter.openWeightFragment(getActivity());
                return true;
            case R.id.action_back:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
