package com.imaknow.calculatebmi.ui.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.CalculateBmiApplication;
import com.imaknow.calculatebmi.interfaces.IMyOnBackPressed;
import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.presenter.MainFragmentPresenter;
import com.imaknow.calculatebmi.ui.pager.GenderFragment;
import com.imaknow.calculatebmi.ui.pager.PagerFragment;
import com.imaknow.calculatebmi.ui.pager.PagerWidget;
import com.vi.swipenumberpicker.OnValueChangeListener;
import com.vi.swipenumberpicker.SwipeNumberPicker;

import java.util.ArrayList;
import java.util.List;

/**
 * Main fragment.
 */
public class MainFragment extends Fragment implements IMyOnBackPressed {

    private View mRootView;
    private List<PagerFragment> mGenderFragmentList;
    private MainFragmentPresenter mMainFragmentPresenter;
    private SwipeNumberPicker mAgeSwipePicker;
    private PagerWidget mGenderPager;
    private TextView mTxtVwAge;

    public MainFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mMainFragmentPresenter = new MainFragmentPresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainFragmentPresenter.setOnBackPressed(getActivity(), this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMainFragmentPresenter.setOnBackPressed(getActivity(), null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);
        initViews();
        generateGenderContent();
        initGenderPager();
        initNumberPicker();
        mMainFragmentPresenter.updateSavedData();

        return mRootView;
    }

    private void initViews() {
        mTxtVwAge = (TextView) mRootView.findViewById(R.id.txt_vw_age);
        mTxtVwAge.setTypeface(mMainFragmentPresenter.getTypeface(getActivity()));
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_next, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                @ApplicationData.Gender String gender = getGenderByPosition(mGenderPager.getCurrentItemPosition());
                mMainFragmentPresenter.saveSelectedValues(gender, mAgeSwipePicker.getValue());
                mMainFragmentPresenter.openHeightFragment(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Creates the pages inside the gender pager.
     */
    private void generateGenderContent() {
        GenderFragment maleFragment = new GenderFragment();
        Bundle args = new Bundle();
        args.putInt(CalculateBmiApplication.BUNDLE_IMAGE_ID, R.drawable.male);
        maleFragment.setArguments(args);

        GenderFragment femaleFragment = new GenderFragment();
        Bundle args2 = new Bundle();
        args2.putInt(CalculateBmiApplication.BUNDLE_IMAGE_ID, R.drawable.female);
        femaleFragment.setArguments(args2);

        mGenderFragmentList = new ArrayList<>();
        mGenderFragmentList.add(maleFragment);
        mGenderFragmentList.add(femaleFragment);
    }

    /**
     * Initializes the gender pager.
     */
    private void initGenderPager() {
        LinearLayout linLayGender = (LinearLayout) mRootView.findViewById(R.id.lin_lay_gender);
        mGenderPager = new PagerWidget(getActivity());
        mGenderPager.setList(getActivity().getSupportFragmentManager(), mGenderFragmentList);
        mGenderPager.setOnPageChangedListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                @ApplicationData.Gender String gender = getGenderByPosition(position);
                mMainFragmentPresenter.saveSelectedGender(gender);
                paintAgePicker(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        linLayGender.addView(mGenderPager);
    }

    private void initNumberPicker() {
        mAgeSwipePicker = (SwipeNumberPicker) mRootView.findViewById(R.id.age_number_picker);
        mAgeSwipePicker.setOnValueChangeListener(new OnValueChangeListener() {
            @Override
            public boolean onValueChange(SwipeNumberPicker view, int oldValue, int newValue) {
                mMainFragmentPresenter.saveSelectedAge(newValue);
                return true;
            }
        });
    }

    public void paintAgePicker(int position) {
        if (position == 0) {
            int maleColor = getResources().getColor(R.color.male_color);
            mAgeSwipePicker.setBackgroundColor(maleColor);
            mMainFragmentPresenter.updateActionBarColor(getActivity(), true);
            mMainFragmentPresenter.updateDrawerColor(getActivity(), true);
        } else {
            int femaleColor = getResources().getColor(R.color.female_color);
            mAgeSwipePicker.setBackgroundColor(femaleColor);
            mMainFragmentPresenter.updateActionBarColor(getActivity(), false);
            mMainFragmentPresenter.updateDrawerColor(getActivity(), false);
        }
    }

    public void setPagerItem(int position) {
        mGenderPager.setPage(position);
    }

    public void setAgeSwipeValue(int value) {
        mAgeSwipePicker.setValue(value, true);
    }

    @NonNull
    private String getGenderByPosition(int position) {
        int genderImage = mGenderFragmentList.get(position).getCurrentValue();
        return genderImage == R.drawable.male ? ApplicationData.GENDER_MALE : ApplicationData.GENDER_FEMALE;
    }

    @Override
    public void onBackPressed() {
        getActivity().finish();
    }
}
