package com.imaknow.calculatebmi.ui.view;

import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.interfaces.IMyOnBackPressed;
import com.imaknow.calculatebmi.presenter.MainActivityPresenter;

public class MainActivity extends AppCompatActivity {
    private MainActivityPresenter mMainPresenter;
    private Toolbar mToolbar;
    private IMyOnBackPressed myOnBackPressed;

    // Drawer
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private TextView mTxtVwDrawerBase;
    private TextView mTxtVwChangeUnits;
    private TextView mTxtVwSettings;
    private TextView mTxtVwInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMainPresenter = new MainActivityPresenter(this);
        configActionBar();

        initViews();

        redirectToFragment();
    }

    private void configActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        boolean isMale = mMainPresenter.isMale();
        updateActionBarColor(isMale);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (myOnBackPressed != null) {
            myOnBackPressed.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void initViews() {
        initDrawer();
    }

    private void initDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTxtVwDrawerBase = (TextView) findViewById(R.id.txt_vw_drawer_base);
        mTxtVwSettings = (TextView) findViewById(R.id.txt_vw_settings);
        mTxtVwInfo = (TextView) findViewById(R.id.txt_vw_info);
        mTxtVwChangeUnits = (TextView) findViewById(R.id.txt_vw_change_unit);
        mTxtVwChangeUnits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainPresenter.openSelectUnitsFragment(MainActivity.this, true);
                closeDrawer();
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        updateDrawerColors(mMainPresenter.isMale());
    }

    private void closeDrawer() {
        mDrawerLayout.closeDrawers();
    }

    public void updateDrawerColors(boolean isMale) {
        if (isMale) {
            mTxtVwDrawerBase.setBackgroundColor(getResources().getColor(R.color.male_color_dark));
            mTxtVwSettings.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_male));
            mTxtVwChangeUnits.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_male));
            mTxtVwInfo.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_male));
        } else {
            mTxtVwDrawerBase.setBackgroundColor(getResources().getColor(R.color.female_color_dark));
            mTxtVwSettings.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_female));
            mTxtVwChangeUnits.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_female));
            mTxtVwInfo.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_item_selector_female));
        }
    }

    public void updateActionBarColor(boolean isMale) {
        if (isMale) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.male_color_dark)));
        } else {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.female_color_dark)));
        }

        paintStatusBar(isMale);
    }

    /**
     * Opens the wanted fragment. The first time allows to select the kind of units the user
     * would like to use.
     */
    private void redirectToFragment() {
        if (mMainPresenter.isUnitSelected()) {
            mMainPresenter.openMainFragment(this, true);
        } else {
            mMainPresenter.openSelectUnitsFragment(this, false);
        }
    }

    /**
     * Changes status bar color for versions Lollipop or higher.
     */
    private void paintStatusBar(boolean isMale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isMale) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.male_color));
            } else {
                getWindow().setStatusBarColor(getResources().getColor(R.color.female_color));
            }
        }
    }

    public void setOnMyBackPressedListener(IMyOnBackPressed onBackPressed) {
        myOnBackPressed = onBackPressed;
    }
}
