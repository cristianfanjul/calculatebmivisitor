package com.imaknow.calculatebmi.ui.pager;

import android.os.Bundle;
import android.widget.TextView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.CalculateBmiApplication;
import com.imaknow.calculatebmi.misc.ApplicationData;

/**
 * Fragment that displays an image for the gender.
 */
public class UnitFragment extends PagerFragment {

    private String mUnitText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        mUnitText = args.getString(CalculateBmiApplication.BUNDLE_UNIT_NAME);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_unit;
    }

    @Override
    public void initViews() {
        TextView txtVwUnit = (TextView) mRootView.findViewById(R.id.txt_vw_unit);
        txtVwUnit.setText(mUnitText);
    }

    @Override
    public String getCurrentValue() {
        return mUnitText;
    }

}