package com.imaknow.calculatebmi.ui.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.presenter.WeightPresenter;
import com.imaknow.calculatebmi.ui.custom.WheelView;

import java.util.List;

/**
 * Height selection fragment.
 */
public class WeightFragment extends Fragment {
    private View mRootView;
    private RelativeLayout mRelLayGeneral;
    private WeightPresenter mWeightPresenter;
    private WheelView mWheelVwWeight;
    private Button mBtnCalculate;
    private TextView mTxtVwWeight;

    private ImageView mLeftArrowImgVw, mRightArrowImgVw;
    private Animation mLeftArrowAnimation, mRightArrowAnimation;
    private List<String> mItems;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mWeightPresenter = new WeightPresenter(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_weight, container, false);
        initViews();

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateWheelLimits();
    }

    private void initViews() {
        mRelLayGeneral = (RelativeLayout) mRootView.findViewById(R.id.rel_lay_weight_general);
        mRelLayGeneral.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mWheelVwWeight.onTouchEvent(event);
            }
        });

        mTxtVwWeight = (TextView) mRootView.findViewById(R.id.txt_vw_weight_selected);

        initWheel();
        initArrowLeft();
        initArrowRight();
        initBtnCalculate();
    }

    private void initWheel() {
        mWheelVwWeight = (WheelView) mRootView.findViewById(R.id.wheel_vw_weight);
        int color = mWeightPresenter.getColorForRoulette();
        mWheelVwWeight.setMarkTextColor(color);
        mWheelVwWeight.setHighlightColor(color);
        fillWheel();
        mWheelVwWeight.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemChanged(WheelView wheelView, int position) {
                drawWeightByPosition(position);
            }

            @Override
            public void onWheelItemSelected(WheelView wheelView, int position) {
                drawWeightByPosition(position);
                String weight = mItems.get(position);
                mWeightPresenter.saveWeight(Integer.valueOf(weight));
            }
        });

        int weight = mWeightPresenter.getWeight();
        mWheelVwWeight.selectIndex(weight - 1);
        mTxtVwWeight.setText(mWeightPresenter.getWeightString(weight));
    }

    private void drawWeightByPosition(int position) {
        String value = mItems.get(position);
        String weight = mWeightPresenter.getWeightString(value);
        mTxtVwWeight.setText(weight);
    }

    private void fillWheel() {
        mItems = mWeightPresenter.getItemsList();
        mWheelVwWeight.setItems(mItems);
        String unit = mWeightPresenter.obtainStringForRoulette();
        mWheelVwWeight.setAdditionCenterMark(unit);
        mWheelVwWeight.invalidate();
        mWheelVwWeight.requestLayout();
    }

    private void initArrowLeft() {
        mLeftArrowImgVw = (ImageView) mRootView.findViewById(R.id.img_vw_double_arrow_left);
        mLeftArrowAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_arrow_left);
        mLeftArrowImgVw.startAnimation(mLeftArrowAnimation);
    }

    private void initArrowRight() {
        mRightArrowImgVw = (ImageView) mRootView.findViewById(R.id.img_vw_double_arrow_right);
        mRightArrowAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_arrow_right);
        mRightArrowImgVw.startAnimation(mRightArrowAnimation);
    }

    private void initBtnCalculate() {
        mBtnCalculate = (Button) mRootView.findViewById(R.id.btn_calculate);
        mBtnCalculate.setTypeface(mWeightPresenter.getTypeface(getActivity()));
        mBtnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void updateWheelLimits() {
        int min = mWeightPresenter.getMinWeightLimit();
        int max = mWeightPresenter.getMaxWeightLimit();
        mWheelVwWeight.setMinSelectableIndex(min);
        mWheelVwWeight.setMaxSelectableIndex(max - 1);
        mWheelVwWeight.selectIndex(mWeightPresenter.getWeight() - 1);
//        if (mWheelVwWeight.getSelectedPosition() > max) {
        // If last selected position is higher than max, the new position is max.
//            mWheelVwWeight.selectIndex(max);
//            mTxtVwWeight.setText(max);
//            mWeightPresenter.saveWeight(max);
//        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_back_next, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                return true;
            case R.id.action_back:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
