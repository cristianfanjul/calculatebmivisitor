package com.imaknow.calculatebmi.ui.pager;

import android.os.Bundle;
import android.widget.ImageView;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.CalculateBmiApplication;

/**
 * Fragment that displays an image for the gender.
 */
public class GenderFragment extends PagerFragment {

    private int mImageId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mImageId = args.getInt(CalculateBmiApplication.BUNDLE_IMAGE_ID);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_gender;
    }

    @Override
    public void initViews() {
        ImageView imageView = (ImageView) mRootView.findViewById(R.id.img_vw_gender);
        imageView.setImageResource(mImageId);
    }

    @Override
    public Integer getCurrentValue() {
        return mImageId;
    }
}