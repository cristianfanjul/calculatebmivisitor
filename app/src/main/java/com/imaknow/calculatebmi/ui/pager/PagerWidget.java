package com.imaknow.calculatebmi.ui.pager;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.imaknow.calculatebmi.interfaces.IPagerControlsListener;
import com.imaknow.calculatebmi.misc.Utils;

import java.util.List;

/**
 * Created by Cristian Fanjul.
 */
public class PagerWidget extends RelativeLayout {

    private static final int INITIAL_PAGE = 0;
    private ViewPager mPager;
    private List<PagerFragment> mFragmentList;
    private int mCurrentPage;

    public PagerWidget(Context context) {
        super(context);
        initViews();
    }

    public PagerWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public PagerWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }


    private void initViews() {
        mCurrentPage = INITIAL_PAGE;
        mPager = new ViewPager(getContext());
        // Add id to ViewPager, which is required to avoid crashes.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mPager.setId(Utils.generateViewId());
        } else {
            mPager.setId(View.generateViewId());
        }
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    for (PagerFragment pagerFragment : mFragmentList) {
                        pagerFragment.hidePagesControlButtons();
                    }
                } else if (state == ViewPager.SCROLL_STATE_IDLE) {
                    for (PagerFragment pagerFragment : mFragmentList) {
                        pagerFragment.showPagesControlButtons();
                    }
                    if (mCurrentPage == INITIAL_PAGE) {
                        showNextButton();
                    } else if (mCurrentPage == mFragmentList.size() - 1) {
                        showBackButton();
                    }
                }
            }
        });

        addView(mPager);
    }

    /**
     * Receives a list of fragments.
     *
     * @param fragmentManager
     * @param fragmentList
     */
    public void setList(FragmentManager fragmentManager, List<PagerFragment> fragmentList) {
        mFragmentList = fragmentList;
        setPagerControlsListeners();
        PagerAdapter adapter = new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }
        };
        mPager.setAdapter(adapter);
    }

    private void setPagerControlsListeners() {
        IPagerControlsListener pagerControlsListener = new IPagerControlsListener() {
            @Override
            public void onClickBack() {
                hideControls();
                mPager.setCurrentItem(mPager.getCurrentItem() - 1, true);
            }

            @Override
            public void onClickNext() {
                hideControls();
                mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
            }
        };
        for (PagerFragment pagerFragment : mFragmentList) {
            pagerFragment.setPagerControlListener(pagerControlsListener);
        }
    }

    /**
     * Hides buttons to go back and forth.
     */
    private void hideControls() {
        for (PagerFragment pagerFragment : mFragmentList) {
            pagerFragment.hidePagesControlButtons();
        }
    }

    /**
     * Displays the back button and hides the one that goes to next element.
     */
    private void showBackButton() {
        mFragmentList.get(mFragmentList.size() - 1).enableBackButton();
        mFragmentList.get(mFragmentList.size() - 1).disableNextButton();
    }

    /**
     * Displays the button next and disables the back one.
     */
    private void showNextButton() {
        mFragmentList.get(INITIAL_PAGE).disableBackButton();
        mFragmentList.get(INITIAL_PAGE).enableNextButton();
    }

    public int getCurrentItemPosition() {
        return mPager.getCurrentItem();
    }

    public void setPage(int pageNumber) {
        mPager.setCurrentItem(pageNumber, true);
        mCurrentPage = pageNumber;

        if (pageNumber == 0) {
            showNextButton();
            mFragmentList.get(pageNumber).setIsFirst(true);
        } else if (pageNumber == mFragmentList.size() - 1) {
            showBackButton();
            mFragmentList.get(pageNumber).setIsLast(true);
        }
    }

    public void setOnPageChangedListener(ViewPager.OnPageChangeListener onPageChangedListener) {
        mPager.addOnPageChangeListener(onPageChangedListener);
    }
}
