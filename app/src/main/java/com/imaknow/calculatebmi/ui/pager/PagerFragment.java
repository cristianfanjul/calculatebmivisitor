package com.imaknow.calculatebmi.ui.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.interfaces.IPagerControlsListener;

/**
 * Fragment to be used as item of the pager inside the widget.
 */
public abstract class PagerFragment extends Fragment {

    protected View mRootView;
    protected ImageButton mImgBtnBack, mImgBtnNext;
    private IPagerControlsListener mPagerControlListener;
    private boolean mIsFirst = true;
    private boolean mIsLast = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(), container, false);

        mImgBtnBack = (ImageButton) mRootView.findViewById(R.id.img_btn_back);
        mImgBtnNext = (ImageButton) mRootView.findViewById(R.id.img_btn_next);
        setBtnListeners();

        initViews();

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mIsFirst) {
            disableBackButton();
            enableNextButton();
        } else if (mIsLast) {
            enableBackButton();
            disableNextButton();
        } else {
            showPagesControlButtons();
        }
    }

    private void setBtnListeners() {
        mImgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPagerControlListener != null) {
                    mPagerControlListener.onClickBack();
                }
            }
        });

        mImgBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPagerControlListener != null) {
                    mPagerControlListener.onClickNext();
                }
            }
        });
    }

    public abstract int getLayoutId();

    public abstract void initViews();

    public void enableNextButton() {
        if (mImgBtnNext != null) {
            mImgBtnNext.setVisibility(View.VISIBLE);
        }
    }

    public void enableBackButton() {
        if (mImgBtnBack != null) {
            mImgBtnBack.setVisibility(View.VISIBLE);
        }
    }

    public void disableNextButton() {
        if (mImgBtnNext != null) {
            mImgBtnNext.setVisibility(View.GONE);
        }
    }

    public void disableBackButton() {
        if (mImgBtnBack != null) {
            mImgBtnBack.setVisibility(View.GONE);
        }
    }

    public void hidePagesControlButtons() {
        if (mImgBtnBack != null && mImgBtnNext != null) {
            mImgBtnBack.setVisibility(View.GONE);
            mImgBtnNext.setVisibility(View.GONE);
        }
    }

    public void showPagesControlButtons() {
        if (mImgBtnBack != null && mImgBtnNext != null) {
            mImgBtnBack.setVisibility(View.VISIBLE);
            mImgBtnNext.setVisibility(View.VISIBLE);
        }
    }

    public void setPagerControlListener(IPagerControlsListener pagerControlListener) {
        mPagerControlListener = pagerControlListener;
    }

    public abstract <T extends Object> T getCurrentValue();

    public void setIsFirst(boolean value) {
        mIsFirst = value;
        if (value == true) {
            mIsLast = false;
        }
    }

    public void setIsLast(boolean value) {
        mIsLast = value;
        if (value == true) {
            mIsFirst = false;
        }
    }
}
