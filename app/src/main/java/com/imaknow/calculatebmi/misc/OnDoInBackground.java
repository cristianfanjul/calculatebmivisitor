package com.imaknow.calculatebmi.misc;

/**
 * Created by Cristian Fanjul on 09/06/2016.
 */
public interface OnDoInBackground {
    void doInBackground();
}
