package com.imaknow.calculatebmi.misc;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.util.Log;

import com.imaknow.calculatebmi.CalculateBmiApplication;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Cristian Fanjul on 01/08/2015.
 */
public class ApplicationData {

    private static final String PREFERENCES_FILE_NAME = "CalculateBMI";
    public static final String ANDROID_FONT = "fonts/Roboto-Regular.ttf";
    public static final int MIN_HEIGHT_CM = 120;
    public static final int MAX_HEIGHT_CM = 220;
    public static final int MIN_HEIGHT_INCHES = 47;
    public static final int MAX_HEIGHT_INCHES = 87;

    public static final int MIN_WEIGHT_KG = 1;
    public static final int MAX_WEIGHT_KG = 250;
    public static final int MIN_WEIGHT_POUNDS = 1;
    public static final int MAX_WEIGHT_POUNDS = 550;

    private static final int DEFAULT_AGE = 25;
    private static final int DEFAULT_HEIGHT_CM = 160;
    private static final int DEFAULT_HEIGHT_INCH = 63;
    private static final int DEFAULT_WEIGHT_KG = 65;
    private static final int DEFAULT_WEIGHT_POUND = 132;
    private static final float ONE_CM_TO_INCHES = 0.393701f;
    private static final float ONE_KG_TO_POUND = 2.20462f;

    private final int PRIVACY_MODE = Activity.MODE_PRIVATE;
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    private static final String OPEN_UNITS_FIRST_TIME = "OpenUnitsFirstTime";
    private static final String UNIT_SELECTED = "UnitsSelected";
    private static final String GENDER_SELECTED = "GenderSelected";
    private static final String AGE_SELECTED = "AgeSelected";
    private static final String HEIGHT_SELECTED = "HeightSelected";
    private static final String WEIGHT_SELECTED = "WeightSelected";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({UNIT_TYPE_KG_CM, UNIT_TYPE_POUND_INCHES})
    public @interface UnitType {
    }

    public static final int UNIT_TYPE_KG_CM = 0;
    public static final int UNIT_TYPE_POUND_INCHES = 1;

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({GENDER_MALE, GENDER_FEMALE})
    public @interface Gender {
    }

    public static final String GENDER_MALE = "GenderMale";
    public static final String GENDER_FEMALE = "GenderFemale";

    public ApplicationData(Context context) {
        appSharedPrefs = context.getSharedPreferences(PREFERENCES_FILE_NAME, PRIVACY_MODE);
        prefsEditor = appSharedPrefs.edit();
    }

    public void persistOpenedFirstTime(boolean value) {
        prefsEditor.putBoolean(OPEN_UNITS_FIRST_TIME, value).apply();
    }

    public boolean isOpenedFirstTime() {
        return appSharedPrefs.getBoolean(OPEN_UNITS_FIRST_TIME, true);
    }

    public void persistUnitType(@UnitType int unitType) {
        convertUnits(getUnitType() == UNIT_TYPE_KG_CM ? UNIT_TYPE_KG_CM : UNIT_TYPE_POUND_INCHES, unitType);
        prefsEditor.putInt(UNIT_SELECTED, unitType).apply();
    }

    public int getUnitType() {
        return appSharedPrefs.getInt(UNIT_SELECTED, UNIT_TYPE_KG_CM);
    }

    public void persistGender(@Gender String gender) {
        prefsEditor.putString(GENDER_SELECTED, gender).apply();
    }

    public String getGender() {
        return appSharedPrefs.getString(GENDER_SELECTED, GENDER_MALE);
    }

    public void persistAge(int age) {
        prefsEditor.putInt(AGE_SELECTED, age).apply();
    }

    public int getAge() {
        return appSharedPrefs.getInt(AGE_SELECTED, DEFAULT_AGE);
    }

    public void persistHeight(int height) {
        prefsEditor.putInt(HEIGHT_SELECTED, height).apply();
    }

    public int getHeight() {
        int defaultHeight = getUnitType() == UNIT_TYPE_KG_CM ? DEFAULT_HEIGHT_CM : DEFAULT_HEIGHT_INCH;
        return appSharedPrefs.getInt(HEIGHT_SELECTED, defaultHeight);
    }

    public void persistWeight(int weight) {
        prefsEditor.putInt(WEIGHT_SELECTED, weight).apply();
    }

    public int getWeight() {
        int defaultWeight = getUnitType() == UNIT_TYPE_KG_CM ? DEFAULT_WEIGHT_KG : DEFAULT_WEIGHT_POUND;
        return appSharedPrefs.getInt(WEIGHT_SELECTED, defaultWeight);
    }

    private void convertUnits(@UnitType int oldUnit, @UnitType int newUnit) {
        if (oldUnit != newUnit) {
            int oldHeight = getHeight();
            int oldWeight = getWeight();
            int convertedHeight, convertedWeight;
            if (newUnit == UNIT_TYPE_KG_CM) {
                // Convert from INCH to CM
                convertedHeight = getInchToCmConvertion(oldHeight);
                persistHeight(convertedHeight);

                // Convert from POUND to KG
                convertedWeight = getPoundToKgConvertion(oldWeight);
                persistWeight(convertedWeight);
            } else {
                // Convert from CM to INCH
                convertedHeight = getCmToInchConvertion(oldHeight);
                persistHeight(convertedHeight);

                // Convert from KG to POUND
                convertedWeight = getKgToPoundConvertion(oldWeight);
                persistWeight(convertedWeight);
            }
        }
    }

    /**
     * Converts inches to centimeters.
     *
     * @param height Value.
     * @return Integer.
     */
    private int getInchToCmConvertion(int height) {
        int convertedHeight = (int) Math.ceil(height / ONE_CM_TO_INCHES);
        int result = convertedHeight;
        if (convertedHeight < MIN_HEIGHT_CM) {
            result = MIN_HEIGHT_CM;
        } else if (convertedHeight > MAX_HEIGHT_CM) {
            result = MAX_HEIGHT_CM;
        }
        return result;
    }

    /**
     * Converts centimeters to inches.
     *
     * @param height Value.
     * @return Integer.
     */
    private int getCmToInchConvertion(int height) {
        int convertedHeight = (int) Math.ceil(height * ONE_CM_TO_INCHES);
        int result = convertedHeight;
        if (convertedHeight < MIN_HEIGHT_INCHES) {
            result = MIN_HEIGHT_INCHES;
        } else if (convertedHeight > MAX_HEIGHT_INCHES) {
            result = MAX_HEIGHT_INCHES;
        }
        return result;
    }

    /**
     * Converts pokilogramsunds to centimeters.
     *
     * @param weight Value.
     * @return Integer.
     */
    private int getPoundToKgConvertion(int weight) {
        int convertedWeight = (int) Math.ceil(weight / ONE_KG_TO_POUND);
        int result = convertedWeight;
        if (convertedWeight < MIN_WEIGHT_KG) {
            result = MIN_WEIGHT_KG;
        } else if (convertedWeight > MAX_WEIGHT_KG) {
            result = MAX_WEIGHT_KG;
        }
        return result;
    }

    private int getKgToPoundConvertion(int weight) {
        int convertedWeight = (int) Math.ceil(weight * ONE_KG_TO_POUND);
        int result = convertedWeight;
        if (convertedWeight < MIN_WEIGHT_POUNDS) {
            result = MIN_WEIGHT_POUNDS;
        } else if (convertedWeight > MAX_WEIGHT_POUNDS) {
            result = MAX_WEIGHT_POUNDS;
        }
        return result;
    }

    private void log(String s) {
        Log.d(CalculateBmiApplication.TAG, s);
    }
}
