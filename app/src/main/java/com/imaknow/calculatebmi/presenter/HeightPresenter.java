package com.imaknow.calculatebmi.presenter;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.HeightFragment;

/**
 * Created by Cristian Fanjul on 03/07/2016.
 */
public class HeightPresenter extends BasePresenter {
    private HeightFragment mHeightFragmentView;

    public HeightPresenter(HeightFragment heightFragmentView) {
        super(heightFragmentView.getContext());
        mHeightFragmentView = heightFragmentView;
    }

    /**
     * Retrieves the unit type is String format.
     *
     * @return unit type in String.
     */
    public String getUnitType() {
        String unitType;
        if (mAppData.getUnitType() == ApplicationData.UNIT_TYPE_KG_CM) {
            unitType = mHeightFragmentView.getContext().getString(R.string.cms);
        } else {
            unitType = mHeightFragmentView.getContext().getString(R.string.inches);
        }
        return unitType;
    }

    public void saveHeight(int height) {
        mAppData.persistHeight(height);
    }

    /**
     * Consider min and max: 120 to 220 for cm.
     * 47 to 87 inches.
     *
     * @param progress Progress in seek bar.
     * @return Height for the person.
     */
    public int getCalculatedHeight(int progress) {
        int result;
        if (mAppData.getUnitType() == ApplicationData.UNIT_TYPE_KG_CM) {
            result = progress + 120;
        } else {
            // 87 - 47 = 40. Every unit of progress is 0.4 from min to max.
            result = (int) Math.ceil(progress * 0.4f + 47);
        }
        return result;
    }

    /**
     * Retrieves how much progress should the bar show. Calculates this value
     * by taking the height and subtracting the factor added (120 for cms and
     * progress * 0.4f + 47 for inches.
     *
     * @return Progress for seek bar.
     */
    public int getProgressFromHeight() {
        int result;
        int height = mAppData.getHeight();
        if (mAppData.getUnitType() == ApplicationData.UNIT_TYPE_KG_CM) {
            result = height - 120;
        } else {
            result = (int) ((height - 47) / 0.4f);
        }
        return result;
    }
}
