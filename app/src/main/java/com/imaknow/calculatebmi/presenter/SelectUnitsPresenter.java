package com.imaknow.calculatebmi.presenter;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.SelectUnitsFragment;

/**
 * Created by Cristian Fanjul on 04/06/2016.
 */
public class SelectUnitsPresenter extends BasePresenter {

    private final SelectUnitsFragment mSelectUnitsFragmentView;

    public SelectUnitsPresenter(SelectUnitsFragment selectUnitsFragmentView) {
        super(selectUnitsFragmentView.getContext());
        mSelectUnitsFragmentView = selectUnitsFragmentView;
    }

    public void saveUnitByValue(String unitValue) {
        @ApplicationData.UnitType final int unitTypeSelected;
        if (unitValue.equals(mSelectUnitsFragmentView.getString(R.string.kg_cm))) {
            unitTypeSelected = ApplicationData.UNIT_TYPE_KG_CM;
        } else {
            unitTypeSelected = ApplicationData.UNIT_TYPE_POUND_INCHES;
        }
        saveUnitSelected(unitTypeSelected);

    }

    /**
     * Saves the unit value selected.
     *
     * @param unitSelected Object that saves preference values.
     */
    public void saveUnitSelected(@ApplicationData.UnitType int unitSelected) {
        if (isOpenedFirstTime()) {
            mAppData.persistOpenedFirstTime(false);
        }

        mAppData.persistUnitType(unitSelected);
    }
}
