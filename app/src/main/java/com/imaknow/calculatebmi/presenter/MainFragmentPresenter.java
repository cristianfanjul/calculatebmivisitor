package com.imaknow.calculatebmi.presenter;

import android.os.AsyncTask;

import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.MainFragment;

/**
 * Created by Cristian Fanjul on 04/06/2016.
 */
public class MainFragmentPresenter extends BasePresenter {

    private MainFragment mMainFragment;
    private String mGender;
    private int mAge;

    public MainFragmentPresenter(MainFragment mainFragmentView) {
        super(mainFragmentView.getContext());
        mMainFragment = mainFragmentView;
    }

    public void updateSavedData() {
        new SavedData().execute();
    }

    public void saveSelectedAge(int age) {
        mAppData.persistAge(age);
    }

    public int getGenderPosition(String gender) {
        if (gender.equals(ApplicationData.GENDER_MALE)) {
            return 0;
        } else {
            return 1;
        }
    }

    @ApplicationData.Gender
    public String getGender() {
        return mGender;
    }

    public int getSavedAge() {
        return mAge;
    }

    public void saveSelectedGender(@ApplicationData.Gender String gender) {
        mAppData.persistGender(gender);
    }

    public void saveSelectedValues(@ApplicationData.Gender String gender, int age) {
        mAppData.persistGender(gender);
        mAppData.persistAge(age);
    }

    private class SavedData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            mAge = mAppData.getAge();
            mGender = mAppData.getGender();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            int genderPosition = getGenderPosition(mGender);
            mMainFragment.setAgeSwipeValue(mAge);
            mMainFragment.setPagerItem(genderPosition);
            mMainFragment.paintAgePicker(genderPosition);
        }
    }
}
