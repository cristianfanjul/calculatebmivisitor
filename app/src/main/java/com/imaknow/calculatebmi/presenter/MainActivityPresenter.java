package com.imaknow.calculatebmi.presenter;

import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.MainActivity;

/**
 * Created by Cristian Fanjul on 04/06/2016.
 */
public class MainActivityPresenter extends BasePresenter {

    private MainActivity mMainActivityView;

    public MainActivityPresenter(MainActivity mainActivityView) {
        super(mainActivityView);
        mMainActivityView = mainActivityView;
    }

    /**
     * Verifies if the user has selected the unit to make the calculations or not.
     *
     * @return boolean True if the units were selected, otherwise false.
     */
    public boolean isUnitSelected() {
        ApplicationData applicationData = new ApplicationData(mMainActivityView);
        if (applicationData.isOpenedFirstTime()) {
            return false;
        } else {
            return true;
        }
    }
}
