package com.imaknow.calculatebmi.presenter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.interfaces.IMyOnBackPressed;
import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.HeightFragment;
import com.imaknow.calculatebmi.ui.view.MainActivity;
import com.imaknow.calculatebmi.ui.view.MainFragment;
import com.imaknow.calculatebmi.ui.view.SelectUnitsFragment;
import com.imaknow.calculatebmi.ui.view.WeightFragment;

/**
 * Created by Cristian Fanjul on 16/07/2016.
 */
public abstract class BasePresenter {
    protected ApplicationData mAppData;

    protected BasePresenter(Context context) {
        mAppData = new ApplicationData(context);
    }

    /**
     * Returns the type of unit selected.
     *
     * @return 0 or 1, which is a constant inside the application.
     */
    public int getCurrentUnitSelected() {
        return mAppData.getUnitType();
    }

    /**
     * Returns the saved height.
     *
     * @return Height.
     */
    public int getHeight() {
        return mAppData.getHeight();
    }

    /**
     * Returns the saved weight.
     *
     * @return Weight.
     */
    public int getWeight() {
        return mAppData.getWeight();
    }

    /**
     * Verifies if the unit selected is Kg and Cm.
     *
     * @return true or false.
     */
    public boolean isCurrentUnitKgAndCm() {
        boolean result;
        if (getCurrentUnitSelected() == ApplicationData.UNIT_TYPE_KG_CM) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public boolean isMale() {
        boolean isMale;
        String gender = mAppData.getGender();
        if (gender.equals(ApplicationData.GENDER_MALE)) {
            isMale = true;
        } else {
            isMale = false;
        }
        return isMale;
    }

    public boolean isOpenedFirstTime() {
        return mAppData.isOpenedFirstTime();
    }

    public void setOnBackPressed(Activity activity, IMyOnBackPressed onBackPressed) {
        if (activity instanceof AppCompatActivity) {
            ((MainActivity) activity).setOnMyBackPressedListener(onBackPressed);
        }
    }

    public void openMainFragment(Activity activity, boolean addToBackStack) {
        openFragment(activity, new MainFragment(), MainFragment.class.getName(), addToBackStack);
    }

    public void openSelectUnitsFragment(Activity activity, boolean addToBackStack) {
        if (!isFragmentOpened(activity, SelectUnitsFragment.class.getName())) {
            openFragment(activity, new SelectUnitsFragment(), SelectUnitsFragment.class.getName(), addToBackStack);
        }
    }

    public void openHeightFragment(Activity activity) {
        openFragment(activity, new HeightFragment(), HeightFragment.class.getName(), true);
    }

    public void openWeightFragment(Activity activity) {
        openFragment(activity, new WeightFragment(), WeightFragment.class.getName(), true);
    }

    private void openFragment(Activity activity, Fragment fragment, String tag, boolean addToBackStack) {
        if (activity instanceof AppCompatActivity) {
            FragmentManager fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_layout, fragment, tag);
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(tag);
            }
            fragmentTransaction.commit();
        }
    }

    public void clearFragmentBackstack(Activity activity) {
        if (activity instanceof AppCompatActivity) {
            FragmentManager fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }
        }
    }

    /**
     * Checks if a fragment was already opened.
     *
     * @param activity
     * @param tag
     * @return
     */
    public boolean isFragmentOpened(Activity activity, String tag) {
        boolean result = false;
        if (activity instanceof AppCompatActivity) {
            FragmentManager fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();
            Fragment myFragment = fragmentManager.findFragmentByTag(tag);
            if (myFragment != null && myFragment.isVisible()) {
                result = true;
            }
        }
        return result;
    }


    public Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), ApplicationData.ANDROID_FONT);
    }

    public void updateActionBarColor(Activity activity, boolean isMale) {
        if (activity instanceof AppCompatActivity) {
            ((MainActivity) activity).updateActionBarColor(isMale);
        }
    }

    public void updateDrawerColor(Activity activity, boolean isMale) {
        if (activity instanceof AppCompatActivity) {
            ((MainActivity) activity).updateDrawerColors(isMale);
        }
    }
}
