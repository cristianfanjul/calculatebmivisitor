package com.imaknow.calculatebmi.presenter;

import android.support.annotation.NonNull;

import com.CalcularPesoIdeal.Inicio.R;
import com.imaknow.calculatebmi.misc.ApplicationData;
import com.imaknow.calculatebmi.ui.view.WeightFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristian Fanjul on 03/07/2016.
 */
public class WeightPresenter extends BasePresenter {
    private WeightFragment mWeightFragmentView;

    public WeightPresenter(WeightFragment heightFragmentView) {
        super(heightFragmentView.getContext());
        mWeightFragmentView = heightFragmentView;
    }

    public void saveWeight(int weight) {
        mAppData.persistWeight(weight);
    }

    public int getMinWeightLimit() {
        int result;
        if (isCurrentUnitKgAndCm()) {
            result = ApplicationData.MIN_WEIGHT_KG;
        } else {
            result = ApplicationData.MIN_HEIGHT_INCHES;
        }
        return result;
    }

    public int getMaxWeightLimit() {
        int result;
        if (isCurrentUnitKgAndCm()) {
            result = ApplicationData.MAX_WEIGHT_KG;
        } else {
            result = ApplicationData.MAX_WEIGHT_POUNDS;
        }
        return result;
    }

    public String getWeightString(int weight) {
        String weightString = String.valueOf(weight);
        return getWeightString(weightString);
    }

    public String getWeightString(String weight) {
        int unit;
        if (isCurrentUnitKgAndCm()) {
            unit = R.string.kg;
        } else {
            unit = R.string.lb;
        }
        return weight + " " + mWeightFragmentView.getResources().getString(unit);
    }

    /**
     * Builds the list of items that the wheel will contain.
     *
     * @return List of Strings.
     */
    public List<String> getItemsList() {
        List<String> items = new ArrayList<>();
        int min, max;
        if (isCurrentUnitKgAndCm()) {
            min = ApplicationData.MIN_WEIGHT_KG;
            max = ApplicationData.MAX_WEIGHT_KG;
        } else {
            min = ApplicationData.MIN_WEIGHT_POUNDS;
            max = ApplicationData.MAX_WEIGHT_POUNDS;
        }
        for (int i = min; i <= max; i++) {
            items.add(String.valueOf(i));
        }
        return items;
    }

    @NonNull
    public String obtainStringForRoulette() {
        String unit;
        if (isCurrentUnitKgAndCm()) {
            unit = mWeightFragmentView.getResources().getString(R.string.kg);
        } else {
            unit = mWeightFragmentView.getResources().getString(R.string.lb);
        }
        return unit;
    }

    /**
     * Returns color for roulette according to gender.
     *
     * @return Color.
     */
    public int getColorForRoulette() {
        int color;
        if (isMale()) {
            color = mWeightFragmentView.getResources().getColor(R.color.male_color_roulette);
        } else {
            color = mWeightFragmentView.getResources().getColor(R.color.female_color_roulette);
        }
        return color;
    }
}
