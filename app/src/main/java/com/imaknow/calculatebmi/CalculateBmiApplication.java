package com.imaknow.calculatebmi;

import android.app.Application;

/**
 * Created by Cristian Fanjul on 21/05/2016.
 */
public class CalculateBmiApplication extends Application {
    public static final String TAG = "CalculateBmiApplication";
    public static final String BUNDLE_IMAGE_ID = "ImageId";
    public static final String BUNDLE_UNIT_NAME = "UnitName";
}
