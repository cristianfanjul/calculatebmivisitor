package com.imaknow.calculatebmi.interfaces;

/**
 * Created by Cristian Fanjul on 16/07/2016.
 */
public interface IMyOnBackPressed {
    void onBackPressed();
}
