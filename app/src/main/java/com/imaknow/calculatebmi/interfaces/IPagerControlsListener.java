package com.imaknow.calculatebmi.interfaces;

/**
 * Created by Cristian Fanjul on 21/05/2016.
 */
public interface IPagerControlsListener {
    void onClickBack();

    void onClickNext();
}
